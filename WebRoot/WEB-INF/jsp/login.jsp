<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="${pageContext.request.contextPath }/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="${pageContext.request.contextPath }/css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="${pageContext.request.contextPath }/css/font-awesome.css" rel="stylesheet">
<!-- jQuery -->
<link
	href='https://fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400'
	rel='stylesheet' type='text/css'>
<!-- lined-icons -->
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<script src="js/jquery-1.10.2.min.js"></script>
<!--clock init-->
</head>
<body>
	<!--/login-->

	<div class="error_page">
		<!--/login-top-->

		<div class="error-top">		
			<h2 class="inner-tittle page">APP信息管理平台</h2>
			<div class="login">
				<h3 class="inner-tittle t-inner">Login</h3>
				<div class="buttons login">
					<ul>
						<li><a href="#" class="hvr-sweep-to-right">后台管理系统</a></li>
						<li class="lost"><a href="#" class="hvr-sweep-to-left">开发者平台</a>
						</li>
						<div class="clearfix"></div>
					</ul>
				</div>
				<form method="post" action="${pageContext.request.contextPath }/user/dologin">
					<input type="text" class="text" value="User" name="userCode"
						onfocus="this.value = '';"
						onblur="if (this.value == '') {this.value = 'UserCode';}">
					<input type="password" value="Password" name="userPassword" onfocus="this.value = '';"
						onblur="if (this.value == '') {this.value = 'Password';}">
					<div class="submit">
						<input type="submit"  value="Login">
					</div>
					<div class="clearfix"></div>

					<div class="new">
						<p>
							<label class="checkbox11"><input type="checkbox"
								name="checkbox"><i> </i>Forgot Password ?</label>
						</p>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>


		</div>
		<!--//login-top-->
	</div>

	<!--/404-->
	<!--js -->
	<script src="${pageContext.request.contextPath }/js/jquery.nicescroll.js"></script>
	<script src="${pageContext.request.contextPath }/js/scripts.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="${pageContext.request.contextPath }/js/bootstrap.min.js"></script>
</body>
</html>