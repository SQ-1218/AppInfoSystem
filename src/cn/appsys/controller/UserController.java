package cn.appsys.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cn.appsys.pojo.BackendUser;
import cn.service.backenduser.BackendUserService;

@Controller
@RequestMapping("/user")
public class UserController {
	@Resource
	private BackendUserService backendUserService;
	
	@RequestMapping(value="/login")
	public String login(){
		return "login";
	}
	@RequestMapping(value="/dologin",method=RequestMethod.POST)
	public String doLogin(@RequestParam String userCode,@RequestParam String userPassword,HttpServletRequest request,HttpSession session){
		//调用service方法，进行用户匹配
		BackendUser user =  backendUserService.login(userCode,userPassword);
		if(null!=user){
			//放入session
			session.setAttribute("userSession", user);
			
			//页面跳转
			return"redirect:/user/main";
		}else {
			return "login";		
		}
	}
	
	@RequestMapping(value="/main")
	public String main(HttpSession session){
		if(session.getAttribute("userSession")==null){
			return"redirect:/user/login";
		}
		return "index";
	}
	
	@RequestMapping(value="/logout")
	public String logout(HttpSession session){
		//清除session
		 if(session.getAttribute("userSession")!=null){
				session.removeAttribute("userSession");
			}
		return "redirect:/user/login";
	}
}
