package cn.service.backenduser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.appsys.pojo.BackendUser;
import cn.dao.backenduser.BackendUserMapper;

@Transactional
@Service("BackendUserService")
public class BackendUserServiceImpl implements BackendUserService{

	@Autowired
	private BackendUserMapper backendUserMapper;
	
	//��¼
	public BackendUser login(String userName, String userPassword) {
		// TODO Auto-generated method stub
		return backendUserMapper.login(userName, userPassword);
	}

}
