package cn.service.devuser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.appsys.pojo.DevUser;
import cn.dao.devuser.DevUserMapper;
@Service("DevUserService")
public class DevUserServicempl implements DevUserService{
	@Autowired
	private DevUserMapper devUserMapper;

	@Override
	public DevUser getLoginUser(String devCode, String devPassword) {
		// TODO Auto-generated method stub
		return devUserMapper.getLoginUser(devCode, devPassword);
	}

}
